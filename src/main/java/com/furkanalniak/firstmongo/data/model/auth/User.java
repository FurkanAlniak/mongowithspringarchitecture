package com.furkanalniak.firstmongo.data.model.auth;

import com.furkanalniak.firstmongo.data.model.AbstractEntity;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@Document
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class User extends AbstractEntity {

    //@Id
    //private String _id;

    @Indexed(unique = true)
    private String username;

    private String textVal;


}
