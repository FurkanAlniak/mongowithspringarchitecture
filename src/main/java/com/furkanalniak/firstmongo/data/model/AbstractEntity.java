package com.furkanalniak.firstmongo.data.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.Version;
import org.springframework.data.domain.Persistable;
import java.time.LocalDateTime;
import java.util.UUID;

public abstract class AbstractEntity implements Persistable<String>,GenericEntity, Cloneable {

  @Transient private static final long serialVersionUID = 1L;

  @Id
  private String _id;

  @Version
  private Long version = 0L;

  private LocalDateTime createDate = LocalDateTime.now();

  @Transient private Boolean isSimpleEntity = Boolean.FALSE;

  @Override
  public boolean isNew() {
    return (getId() == null);
  }

  @Override
  public String  getId() {
    return this._id;
  }

  @Override
  public void setId(String  id) {
    this._id = id;
  }

  @Override
  public Long getVersion() {
    return version;
  }

  @Override
  public void setVersion(Long version) {
    this.version = version;
  }

  @Override
  public Boolean getIsSimpleEntity() {
    return isSimpleEntity;
  }

  @Override
  public void setIsSimpleEntity(Boolean isSimpleEntity) {
    this.isSimpleEntity = isSimpleEntity;
  }

  @Override
  public LocalDateTime getCreateDate() {
    return createDate;
  }

  @Override
  public void setCreateDate(LocalDateTime createDate) {
    this.createDate = createDate;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((_id == null) ? 0 : _id.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    AbstractEntity other = (AbstractEntity) obj;
    if (_id == null) {
      if (other._id != null) return false;
    } else if (!_id.equals(other._id)) return false;
    return true;
  }

  @Override
  public Object clone() throws CloneNotSupportedException {
    return super.clone();
  }
}
