package com.furkanalniak.firstmongo.data.model;

import java.io.Serializable;
import java.time.LocalDateTime;

public interface GenericEntity extends Serializable {

    String  getId();
    void setId(String  id);
    Long getVersion();
    void setVersion(Long version);
    Boolean getIsSimpleEntity();
    void setIsSimpleEntity(Boolean isSimpleEntity);
    LocalDateTime getCreateDate();
    void setCreateDate(LocalDateTime createDate);
}
