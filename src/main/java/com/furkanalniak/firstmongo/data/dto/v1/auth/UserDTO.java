package com.furkanalniak.firstmongo.data.dto.v1.auth;

import com.furkanalniak.firstmongo.data.dto.AbstractDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserDTO extends AbstractDTO {

 //private String _id;

  private String username;

  private String textVal;
}
