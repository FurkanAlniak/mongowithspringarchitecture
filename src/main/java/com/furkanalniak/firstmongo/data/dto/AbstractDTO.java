package com.furkanalniak.firstmongo.data.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public abstract class AbstractDTO implements GenericDTO {

  private static final long serialVersionUID = 1L;

  private String _id;
  private Long version;
  private LocalDateTime createDate;

  @Override
  public String getId() {
    return this._id;
  }

  @Override
  public void setId(String id) {
    this._id = id;
  }

  @Override
  public Long getVersion() {
    return version;
  }

  @Override
  public void setVersion(Long version) {
    this.version = version;
  }

  @Override
  public LocalDateTime getCreateDate() {
    return createDate;
  }

  @Override
  public void setCreateDate(LocalDateTime createDate) {
    this.createDate = createDate;
  }

  @Override
  public int hashCode() {
    final int prime = 7;
    int result = 1;
    result = prime * result + ((_id == null) ? 0 : _id.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    AbstractDTO other = (AbstractDTO) obj;
    if (_id == null) {
      if (other._id != null) return false;
    } else if (!_id.equals(other._id)) return false;
    return true;
  }
}
