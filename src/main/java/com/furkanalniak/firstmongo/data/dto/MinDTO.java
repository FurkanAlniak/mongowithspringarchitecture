package com.furkanalniak.firstmongo.data.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class MinDTO {
  private String id;
  private String name;
}
