package com.furkanalniak.firstmongo.data.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

public interface GenericDTO extends Serializable {
  String  getId();

  void setId(String id);

  Long getVersion();

  void setVersion(Long version);

  LocalDateTime getCreateDate();

  void setCreateDate(LocalDateTime createDate);
}
