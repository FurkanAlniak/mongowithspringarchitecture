package com.furkanalniak.firstmongo.data.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SimpleDTO extends AbstractDTO {

  private static final long serialVersionUID = 1L;
  private String name;
  private Integer statusCode;
}
