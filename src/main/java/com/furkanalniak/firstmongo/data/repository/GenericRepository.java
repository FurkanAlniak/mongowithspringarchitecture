package com.furkanalniak.firstmongo.data.repository;

import com.furkanalniak.firstmongo.data.model.GenericEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.UUID;

@NoRepositoryBean
public interface GenericRepository<E extends GenericEntity> extends MongoRepository<E, String> {

}
