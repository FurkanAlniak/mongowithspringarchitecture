package com.furkanalniak.firstmongo.data.repository.auth;

import com.furkanalniak.firstmongo.data.model.auth.User;
import com.furkanalniak.firstmongo.data.repository.GenericRepository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends GenericRepository<User> {

}
