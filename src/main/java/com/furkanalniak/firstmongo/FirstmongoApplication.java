package com.furkanalniak.firstmongo;

import com.furkanalniak.firstmongo.data.repository.auth.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

//@EntityScan("com.furkanalniak.firstmongo.data.model.*")
@SpringBootApplication
//@ComponentScan(basePackages = "com")
//@EnableMongoRepositories(basePackages = "com.furkanalniak.firstmongo")
public class FirstmongoApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstmongoApplication.class, args);
	}

}
