package com.furkanalniak.firstmongo.aop;


import com.furkanalniak.firstmongo.exception.AuthenticateException;
import com.furkanalniak.firstmongo.exception.LoginFailException;
import com.furkanalniak.firstmongo.exception.PageNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.naming.NoPermissionException;

@ControllerAdvice
public class GlobalRestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> handleGenericException(Exception e, WebRequest request) {
        e.printStackTrace();
        ResponseEntity<Object> responseEntity = customExceptionControl(e);
        if (responseEntity != null) return responseEntity;
        return new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    private ResponseEntity<Object> customExceptionControl(Exception e) {

        if (e instanceof AuthenticateException) {
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        }
        if (e instanceof LoginFailException) {
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.FORBIDDEN);
        }
        if (e instanceof NoPermissionException) {
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.FORBIDDEN);
        }
        if (e instanceof PageNotFoundException) {
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.NOT_FOUND);
        }

        return null;
    }
}
