package com.furkanalniak.firstmongo.api.v1.controller.auth;

import com.furkanalniak.firstmongo.api.v1.controller.AbstractController;
import com.furkanalniak.firstmongo.data.dto.v1.auth.UserDTO;
import com.furkanalniak.firstmongo.data.model.auth.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RequestMapping(value = "/user")
public class UserController extends AbstractController<User, UserDTO> {}
