package com.furkanalniak.firstmongo.api.v1.mapper.auth;


import com.furkanalniak.firstmongo.api.v1.mapper.AbstractMapper;
import com.furkanalniak.firstmongo.data.dto.v1.auth.UserDTO;
import com.furkanalniak.firstmongo.data.model.auth.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper extends AbstractMapper<User, UserDTO> {

    @Override
    protected void fillDTO(UserDTO dto, User entity) {
        //dto.set_id(entity.get_id());
        dto.setUsername(entity.getUsername());
        dto.setTextVal(entity.getTextVal());
    }

    @Override
    protected void fillEntity(User entity, UserDTO dto) {
        //entity.set_id(dto.get_id());
        entity.setUsername(dto.getUsername());
        entity.setTextVal(dto.getTextVal());
    }
}
