package com.furkanalniak.firstmongo.api.v1.controller;

import com.furkanalniak.firstmongo.api.v1.mapper.GenericMapper;
import com.furkanalniak.firstmongo.common.CustomResponse;
import com.furkanalniak.firstmongo.data.dto.GenericDTO;
import com.furkanalniak.firstmongo.data.dto.SimpleDTO;
import com.furkanalniak.firstmongo.data.model.GenericEntity;
import com.furkanalniak.firstmongo.security.SessionContext;
import com.furkanalniak.firstmongo.service.GenericService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public abstract class AbstractController<E extends GenericEntity, D extends GenericDTO> {

  @Autowired protected GenericService<E> service;
  @Autowired protected GenericMapper<E, D> mapper;
  @Autowired protected SessionContext sessionContext;

  protected Gson gson = new Gson();
  protected Type mapType = new TypeToken<Map<String, Object>>() {}.getType();

  @GetMapping("/{id}")
  public ResponseEntity<D> get(@PathVariable String id) throws Exception {
    Optional<E> entity = service.getById(id);
    return new ResponseEntity<D>(mapper.toDTO(entity.get()), HttpStatus.OK);
  }

  @GetMapping("/simple/{id}")
  public ResponseEntity<SimpleDTO> getSimple(@PathVariable String id) throws Exception {
    Optional<E> entity = service.getById(id);
    return new ResponseEntity<>(mapper.toSimpleDTO(entity.get()), HttpStatus.OK);
  }

  @GetMapping
  public ResponseEntity<List<D>> get() throws Exception {
    List<E> entities = service.getAll();
    return new ResponseEntity<List<D>>(mapper.toDTOs(entities), HttpStatus.OK);
  }

  @GetMapping("/simple")
  public ResponseEntity<List<SimpleDTO>> getAllSimpleDTO() throws Exception {
    List<E> entities = service.getAll();
    return new ResponseEntity<List<SimpleDTO>>(mapper.toSimpleDTOs(entities), HttpStatus.OK);
  }

  @GetMapping("/getByIds")
  public ResponseEntity<List<D>> getAllByIds(@RequestParam("ids") List<String > ids) throws Exception {
    List<E> entities = service.getAllByIdList(ids);
    return new ResponseEntity<List<D>>(mapper.toDTOs(entities), HttpStatus.OK);
  }

  @GetMapping("/new")
  public ResponseEntity<D> newEntity() throws Exception {
    E entity = service.newEntity();
    return new ResponseEntity<>(mapper.toDTO(entity), HttpStatus.OK);
  }

  @GetMapping("/copy/{id}")
  public ResponseEntity<D> copy(@PathVariable String id) throws Exception {
    E entity = service.copy(id);
    return new ResponseEntity<>(mapper.toDTO(entity), HttpStatus.OK);
  }

  @PostMapping
  public ResponseEntity<D> save(@RequestBody D dto) throws Exception {
    E entity = service.save(mapper.toEntity(dto));
    return new ResponseEntity<D>(mapper.toDTO(entity), HttpStatus.CREATED);
  }

  @PostMapping("/saveAll")
  public ResponseEntity<List<D>> saveAll(@RequestBody List<D> dtos) throws Exception {
    List<E> entities = service.saveAll(mapper.toEntities(dtos));
    return new ResponseEntity<>(mapper.toDTOs(entities), HttpStatus.CREATED);
  }

  @PutMapping
  public ResponseEntity<?> update(@RequestBody D dto) throws Exception {
    Optional<E> entity = service.getById((dto.getId().toString()));
    if (entity == null) {
      return new ResponseEntity<>(
          new CustomResponse(HttpStatus.NOT_FOUND.toString()), HttpStatus.NOT_FOUND);
    }
    E savedEntity = service.save(mapper.toEntity(dto));
    return new ResponseEntity<>(mapper.toDTO(savedEntity), HttpStatus.OK);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<CustomResponse> deleteById(@PathVariable String id) throws Exception {
    Optional<E> entity = service.getById(id);
    if (entity == null) {
      return new ResponseEntity<>(
          new CustomResponse(HttpStatus.NOT_FOUND.toString()), HttpStatus.NOT_FOUND);
    }
    service.delete(entity.get());
    return new ResponseEntity<>(new CustomResponse(HttpStatus.OK.toString()), HttpStatus.OK);
  }
}
