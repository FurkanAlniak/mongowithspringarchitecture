package com.furkanalniak.firstmongo.exception;

public class PageNotFoundException extends Exception {

  private static final long serialVersionUID = 1L;

  public PageNotFoundException() {}

  public PageNotFoundException(String msg) {
    super(msg);
  }
}
