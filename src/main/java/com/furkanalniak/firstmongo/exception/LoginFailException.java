package com.furkanalniak.firstmongo.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginFailException extends Exception {

  private static final long serialVersionUID = 1L;

  public LoginFailException() {}

  public LoginFailException(String message) {
    super(message);
  }
}
