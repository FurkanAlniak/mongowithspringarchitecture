package com.furkanalniak.firstmongo.exception;

public class AuthenticateException extends Exception {

  private static final long serialVersionUID = 1L;

  public AuthenticateException() {}

  public AuthenticateException(String message) {
    super(message);
  }
}
