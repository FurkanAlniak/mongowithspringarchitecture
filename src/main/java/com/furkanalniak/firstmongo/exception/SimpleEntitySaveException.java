package com.furkanalniak.firstmongo.exception;

public class SimpleEntitySaveException extends Exception {
  private static final long serialVersionUID = 1L;

  public SimpleEntitySaveException() {}

  public SimpleEntitySaveException(String msg) {
    super(msg);
  }
}
