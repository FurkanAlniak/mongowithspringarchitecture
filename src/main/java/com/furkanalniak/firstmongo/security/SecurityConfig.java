package com.furkanalniak.firstmongo.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.HashSet;

@Configuration
public class SecurityConfig {

  /**
   * Authenticationın dışında bırakmak istediğimiz Rest sınıflarımızı burada tanımlıyoruz Sadece GET
   * requestler auhtenticationdan geçer.
   */
  @Bean("publicRequestMappingClassPools")
  public HashMap<String, Boolean> getPublicGetMethodsMappingsClassScope() {
    HashMap<String, Boolean> map = new HashMap<>();
    return map;
  }

  /**
   * Authenticationın dışında bırakmak istediğimiz Rest methodlarımızı burada tanımlıyoruz Sadece
   * GET requestler auhtenticationdan geçer.
   */
  @Bean("publicRequestMappingMethodPools")
  public HashSet<String> getPublicGetMethodsMappingsMethodScope() {
    HashSet<String> set = new HashSet<>();
    return set;
  }
}
