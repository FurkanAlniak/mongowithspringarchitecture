package com.furkanalniak.firstmongo.security;

import com.furkanalniak.firstmongo.data.model.auth.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class JwtUtil {
    @Value("${jwt.secret}")
    private String secret;

    public String encrypt(User user) {
        Claims claims = Jwts.claims().setSubject(String.valueOf(user.getId()));
        return Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS512, secret).compact();
    }

    public User decrypt(String token) {
        User user = null;
        try {
            Claims body = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
            user = new User();
            user.setId((body.getSubject()));
        } catch (Exception e) {
            System.out.println(e);
        }

        return user;
    }
}
