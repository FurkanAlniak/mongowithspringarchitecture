package com.furkanalniak.firstmongo.security;

import com.furkanalniak.firstmongo.exception.AuthenticateException;
import com.furkanalniak.firstmongo.exception.PageNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.HashSet;

@Component
public class AuthManager {
    public static final String AUTHENTICATION_HEADER_NAME = "Authorization";
    public static final String HEADER_PREFIX = "Bearer ";
    public static final String REST_METHOD_COPY = "/copy";

    @Autowired
    JwtUtil jwtUtil;
    @Autowired
    ApplicationContext appContext;

    @Autowired
    @Qualifier("publicRequestMappingClassPools")
    private HashMap<String, Boolean> publicRequestMappingClassPools;

    @Autowired
    @Qualifier("publicRequestMappingMethodPools")
    private HashSet<String> publicRequestMappingMethodPools;

    public void authenticate(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        String requestMappingClassScope = getRequestMappingClassScope(request);
        if (request.getServletPath().startsWith("/user")) return;
        if (publicRequestMappingClassPools.get(requestMappingClassScope) != null
                && HttpMethod.GET.name().equals(request.getMethod())) return;
        String header = request.getHeader(AUTHENTICATION_HEADER_NAME);
        if (header == null || !header.startsWith(HEADER_PREFIX)) {
            throw new AuthenticateException("Token bulunamadi tekrar login olmalisiniz!");
        }
        String token = header.substring(7);
        HttpSession session = request.getSession();
        // Kullanıcı giriş yapmış mı?
        if (session != null && session.getAttribute(token) == null) {
            throw new AuthenticateException("Session bulunamadi tekrar login olmalisiniz!");
        }

        // Kullanıcı girişi zorunlu ama rollerde olması zorunlu olmayan sayfalara izin ver
        if (HttpMethod.GET.name().equals(request.getMethod())
                && isPublicMethod2(request.getServletPath())) return;
        // Kullanıcı izinlerini kontrol et
        checkPermissions(session.getAttribute(token), request, requestMappingClassScope);
    }

    @SuppressWarnings("unchecked")
    private void checkPermissions(Object attribute, HttpServletRequest request, String classScope)
            throws Exception {

        // TODO: Yapılacak.
    }

    private String getRequestMappingClassScope(HttpServletRequest request) throws Exception {
        try {
            RequestMappingHandlerMapping requestMappingHandlerMapping =
                    appContext.getBean(RequestMappingHandlerMapping.class);
            String value =
                    ((HandlerMethod) requestMappingHandlerMapping.getHandler(request).getHandler())
                            .getBeanType()
                            .getAnnotation(RequestMapping.class)
                            .value()[0];
            return value;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new PageNotFoundException(
                    "pageNotFound: "
                            + request.getServletPath()
                            + " Note: check RequestMapping is camel case string");
        }
    }

    private boolean isPublicMethod2(String path) {
        return publicRequestMappingMethodPools.contains(path.substring(path.lastIndexOf("/")));
    }
}
