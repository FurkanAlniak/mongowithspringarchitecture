package com.furkanalniak.firstmongo.config;

import com.mongodb.*;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

@Configuration
public class MongoConfiguration extends AbstractMongoClientConfiguration {

    @Override
    protected String getDatabaseName() {
        return "FirstDatabase";
    }

    @Override
    public MongoClient mongoClient() {
        ConnectionString connectionString = new ConnectionString("mongodb://furkan_admin:mQRczmFCFz5GrIcg@firstcluster-shard-00-00.eca4n.gcp.mongodb.net:27017,firstcluster-shard-00-01.eca4n.gcp.mongodb.net:27017,firstcluster-shard-00-02.eca4n.gcp.mongodb.net:27017/FirstDatabase?ssl=true&replicaSet=atlas-35sfag-shard-0&authSource=admin&retryWrites=true&w=majority");
        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .build();

        return MongoClients.create(mongoClientSettings);
    }

    @Override
    public Collection getMappingBasePackages() {
        return Collections.singleton("com.furkanalniak");
    }
}