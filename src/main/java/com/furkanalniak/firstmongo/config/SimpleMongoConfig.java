package com.furkanalniak.firstmongo.config;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;

@Configuration
public class SimpleMongoConfig {

    @Bean
    public MongoClient mongo() {
        ConnectionString connectionString = new ConnectionString("mongodb://furkan_admin:mQRczmFCFz5GrIcg@firstcluster-shard-00-00.eca4n.gcp.mongodb.net:27017,firstcluster-shard-00-01.eca4n.gcp.mongodb.net:27017,firstcluster-shard-00-02.eca4n.gcp.mongodb.net:27017/FirstDatabase?ssl=true&replicaSet=atlas-35sfag-shard-0&authSource=admin&retryWrites=true&w=majority");
        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .build();

        return MongoClients.create(mongoClientSettings);
    }

    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        return new MongoTemplate(mongo(), "FirstDatabase");
    }
}
