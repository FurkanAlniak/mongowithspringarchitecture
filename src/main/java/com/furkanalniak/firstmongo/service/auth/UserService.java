package com.furkanalniak.firstmongo.service.auth;

import com.furkanalniak.firstmongo.data.model.auth.User;
import com.furkanalniak.firstmongo.service.AbstractService;
import org.springframework.stereotype.Service;

@Service
public class UserService extends AbstractService<User> {

}
