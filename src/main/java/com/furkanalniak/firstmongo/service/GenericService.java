package com.furkanalniak.firstmongo.service;

import com.furkanalniak.firstmongo.data.model.GenericEntity;

import java.util.List;
import java.util.Optional;

public interface GenericService<E extends GenericEntity> {
  E copy(String id) throws Exception;

  E newEntity() throws Exception;

  E save(E entity) throws Exception;

  void delete(E entity) throws Exception;

  void deleteById(String id) throws Exception;

  void deleteAll(List<E> entities) throws Exception;

  Optional<E> getById(String id) throws Exception;

  List<E> saveAll(List<E> entities) throws Exception;

  List<E> getAll() throws Exception;

  List<E> getAllByIdList(List<String > ids) throws Exception;

  void persist(E entity);

}
