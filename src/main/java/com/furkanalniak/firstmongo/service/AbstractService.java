package com.furkanalniak.firstmongo.service;

import com.furkanalniak.firstmongo.data.model.GenericEntity;
import com.furkanalniak.firstmongo.data.repository.GenericRepository;
import com.furkanalniak.firstmongo.exception.SimpleEntitySaveException;
import com.furkanalniak.firstmongo.security.SessionContext;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.GenericTypeResolver;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.util.*;

@Transactional(rollbackFor = Throwable.class)
public abstract class AbstractService<E extends GenericEntity> implements GenericService<E> {

  // @PersistenceContext protected EntityManager em;
  @Autowired protected GenericRepository<E> repository;
  @Autowired protected SessionContext sessionContext;

  protected Gson gson = new Gson();
  protected Type mapType = new TypeToken<Map<String, Object>>() {}.getType();

  @SuppressWarnings("unchecked")
  private Class<E> getSourceTypeEntity() {
    Class<?>[] typeArgs =
        GenericTypeResolver.resolveTypeArguments(getClass(), GenericService.class);
    return (Class<E>) typeArgs[0];
  }

  protected void generateId(E entity) {
    String id = UUID.randomUUID().toString();
    entity.setId(id);
  }
  @Override
   public void persist(E entity) {

    }


  @Override
  public E save(E entity) throws Exception {
    if (entity.getIsSimpleEntity()) {
      throw new SimpleEntitySaveException("Can not save SimpleEntity. Manage entity before save");
    }
    if (entity.getId() == null) {
      generateId(entity);
    }
    return  repository.save(entity);
  }


  @Override
  public List<E> saveAll(List<E> entities) throws Exception {
    entities
        .iterator()
        .forEachRemaining(
            e -> {
              if (e.getIsSimpleEntity()) {
                try {
                  throw new SimpleEntitySaveException(
                      "Can not save SimpleEntity. Manage entity before save");
                } catch (SimpleEntitySaveException ex) {
                  ex.printStackTrace();
                }
              }
            });

    List<E> saveAll = new ArrayList<>();
    for (E entity : entities) {
      saveAll.add(save(entity));
    }
    return saveAll;
  }

  @Override
  public E copy(String id) throws Exception {
    Optional<E> entity = repository.findById(id);
    E entityNew = getSourceTypeEntity().newInstance();
    BeanUtils.copyProperties(entity, entityNew);
    entityNew.setId(null);
    entityNew.setVersion(null);

    return entityNew;
  }

  @Override
  public E newEntity() throws Exception {
    E entity = getSourceTypeEntity().newInstance();
    return entity;
  }

  @Override
  public void delete(E entity) throws Exception {
    repository.delete(entity);
  }

  @Override
  public void deleteAll(List<E> entities) throws Exception {
    this.repository.deleteAll(entities);
  }

  @Override
  public Optional<E> getById(String id) throws Exception {
    return repository.findById(id);
  }

  @Override
  public List<E> getAll() throws Exception {
    return repository.findAll();
  }

  @Override
  public List<E> getAllByIdList(List<String> ids) throws Exception {
    Iterable<E> eIterable = repository.findAllById(ids);
    List<E> entities = new ArrayList<>();
    Iterator<E> eIterator = eIterable.iterator();
    while (eIterator.hasNext()) {
      entities.add(eIterator.next());
    }
    return entities;
  }

  @Override
  public void deleteById(String id) throws Exception {
    repository.deleteById(id);
  }
}
